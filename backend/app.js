'use strict'

var express = require('express');
var bodyParser = require('body-parser');

var app = express();

//cargar archivos de rutas
var project_routes = require('./routes/project');

//middlewares
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

//CORS

//rutas

app.use('/api', project_routes);

/*app.get('/', (req, res) => {
	res.status(200).send(
		"<h1>Página de Inicio</h1>"
	);
});

app.get('/test/:id', (req, res) => {
	console.log(req.body.nombre);
	console.log(req.query.web);
	console.log(req.params.id)

	res.status(200).send({
		message: "Hola Mundo desde mi API de NodeJS"
	});
});*/

//exportar
module.exports = app;